How to duplicate commits :

create branch master with 3 commits.
create branch develop based on master.
add a commit to master, then push to a remote repo
add a commit to develop, then push to a remote repo
localy, rebase develop on master.
try to push to the remote develop branch.

You'll see the following message :


Guillaumes-MBP:04_rebase_duplicating_commits gimbert$ git push origin develop
To gitlab.com:guillaume-m-imbert/04_rebase_duplicating_commits.git
 ! [rejected]        develop -> develop (non-fast-forward)
error: failed to push some refs to 'git@gitlab.com:guillaume-m-imbert/04_rebase_duplicating_commits.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
Guillaumes-MBP:04_rebase_duplicating_commits gimbert$ 





The hint it gives (git pull...) is the error.
If you pull from remote develop you'll duplicate your commits




Guillaumes-MBP:04_rebase_duplicating_commits gimbert$ git log
commit cbd3c3e29bf17ea204d6ebc0a338ed0d06693c19 (HEAD -> develop)
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:27:07 2018 +0100

    C6

commit 68fe184c564574e35d29e98228ecb7d6b913690e
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:26:54 2018 +0100

    C5

commit 5ccd436f4dd42cda70f354e96662b93a09ed5613 (origin/master, master)
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:28:39 2018 +0100

    C7

commit 4da8a77b816c6b5328c6e734da44e213ca260558
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:22:05 2018 +0100

    C4

commit c625d221cdd78754e9040bb0bbbad95faf1d1dc1
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:21:59 2018 +0100

    C3

commit 5bda74beda6a72851ff716701d6b2e8de1f43ae6
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:21:53 2018 +0100

    C2

commit ac321ede3e96694ff7a81ccb6afbebe9b9282b2d
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:21:36 2018 +0100

    C1 
Guillaumes-MBP:04_rebase_duplicating_commits gimbert$ git pull origin develop
From gitlab.com:guillaume-m-imbert/04_rebase_duplicating_commits
 * branch            develop    -> FETCH_HEAD
Merge made by the 'recursive' strategy.
Guillaumes-MBP:04_rebase_duplicating_commits gimbert$ git log
commit d9efe51afc1959620182103c63ca29509775401a (HEAD -> develop)
Merge: cbd3c3e 0b8d1ed
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:57:57 2018 +0100

    Merge branch 'develop' of gitlab.com:guillaume-m-imbert/04_rebase_duplicating_commits into develop

commit cbd3c3e29bf17ea204d6ebc0a338ed0d06693c19
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:27:07 2018 +0100

    C6

commit 68fe184c564574e35d29e98228ecb7d6b913690e
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:26:54 2018 +0100

    C5

commit 5ccd436f4dd42cda70f354e96662b93a09ed5613 (origin/master, master)
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:28:39 2018 +0100

    C7

commit 0b8d1ed664274ee769b7cc5944290ec6c013851d (origin/develop)
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:27:07 2018 +0100

    C6

commit 8e825b010582d27125a6bf545bf6a6c1912e4171
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:26:54 2018 +0100

    C5

commit 4da8a77b816c6b5328c6e734da44e213ca260558
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:22:05 2018 +0100

    C4

commit c625d221cdd78754e9040bb0bbbad95faf1d1dc1
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:21:59 2018 +0100

    C3

commit 5bda74beda6a72851ff716701d6b2e8de1f43ae6
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:21:53 2018 +0100

    C2

commit ac321ede3e96694ff7a81ccb6afbebe9b9282b2d
Author: Guillaume Imbert <guillaume.m.imbert@gmail.com>
Date:   Wed Dec 19 10:21:36 2018 +0100

    C1

